#!/usr/bin/env python3
'''

Reads a file that contains assembly instructions of any architecture, assembles
the instructions, and returns the machine code.

This script can be used in the following manner:
python3 ./solve.py --payload payloads/select.asm
python3 ./solve.py -p payloads/select.asm
python3 ./solve.py -p payloads/execve.asm
python3 ./solve.py -h

Returns:
    The bytecode of the assembled file.
'''

import argparse
from io import TextIOWrapper
from pwn import asm, log, context

context.log_level = 'info'


def generate_shellcode_from_payload(file: TextIOWrapper) -> bytes:
    '''Reads from a file opened from argparse and returns the assembled
    bytecode.
    '''
    payload = file.read()
    machine_code = asm(payload)
    return machine_code


def main() -> bytes:
    '''Return the bytecode from the assembled file.'''

    parser = argparse.ArgumentParser(
        description='Generate shellcode from payload')
    parser.add_argument('-p',
                        '--payload',
                        help='Filename of payload',
                        type=argparse.FileType('r', encoding='ascii'),
                        required=True)
    args = parser.parse_args()

    machine_code = generate_shellcode_from_payload(args.payload)
    log.success(f'Bytecode: {machine_code}')


if __name__ == '__main__':
    main()
