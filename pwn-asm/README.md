# Use pwn.asm to Read A File and Assemble to Machine Code

The purpose this snippet to demonstrate how to open and read in an assembly file
and return the assembled machine code. Using argparse, the user can pass in any
assembly file they wish to read in. It returns the machine code as a bytestring.
The result is logged to the console.

## Usage

```python
python3 ./solve.py <-p/--payload> <file>
```

### Examples
```python
python3 ./solve.py --payload payloads/select.asm
python3 ./solve.py -p payloads/select.asm
python3 ./solve.py -p payloads/execve.asm
python3 ./solve.py -h
```
